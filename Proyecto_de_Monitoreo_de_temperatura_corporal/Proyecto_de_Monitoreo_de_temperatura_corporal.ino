#ifdef ESP32
  #include <WiFi.h>
#else
  #include <ESP8266WiFi.h>
#endif
#include <WiFiClientSecure.h>
#include <UniversalTelegramBot.h>   // Libreria universal del Bot de Telegram
#include <ArduinoJson.h>
#include <Wire.h>
#include <LiquidCrystal_I2C.h>
LiquidCrystal_I2C pantalla(0x27,16,2);
const int analogIn = A0;
 
int RawValue= 0;
double Voltage = 0;
double tempC = 0;

int num = 0;
int contador = 0;
int tiempo = 0;
boolean stoptimer = false;


// credenciales de la red
const char* ssid = "HUAWEI Y9 2019";
const char* password = "agua123w";

// Se inicializa el bot
#define BOTtoken "1309838007:AAH0BkttFRWu8AU3ItEksblkYoZRxqOO894"  // Dentro de las llaves se coloca la API del bot de telegram


#define CHAT_ID "1051483657" // USERID para utilizar el bot de telegram

WiFiClientSecure client;
UniversalTelegramBot bot(BOTtoken, client);




void setup() {
  
  Serial.begin(9600);

 pantalla.init();
 pantalla.backlight();
  

  #ifdef ESP8266
    client.setInsecure();
  #endif

  
  // Connect to Wi-Fi
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.println("Conectando con la Red WI-FI..");
  }
  // Print ESP32 Local IP Address
  Serial.println(WiFi.localIP());

  bot.sendMessage(CHAT_ID, "MONITOR DE TEMPERATURA CORPORAL EN LINEA.\n\n Puedes dar inicio a tu chequeo. :).\n\n Temperatura Corporal TC \n <(Menor) \n >(Mayor) \n\n Rangos y Equivalencia de Temperatura Corporal \n\n -Hipotermia \n TC>41.0 \n -Febre Alta \n TC 39.5 - 41.0.\n -Febre \n TC 37.5 - 39.5.\n -Normal  \n TC 36.0 - 37.5 \n Hipotermia \n TC <35.0.");
}

void loop() {

  contador++;
    
    // Serial.println(tiempo);

     if(contador ==5)
     {
      tiempo++;
       Serial.println(tiempo);
      contador =0;
     }
     if(tiempo == 5)
     {
      stoptimer = true;
      
      }  

RawValue = analogRead(analogIn);
Voltage = (RawValue / 2048.0) * 3300; // 5000 to get millivots.
tempC = Voltage * 0.1;
Serial.print("\t Temperatura in C = ");
Serial.print(tempC,1);
pantalla.setCursor(0,0);
pantalla.print("Su resultado es: ");
pantalla.setCursor(5,1);
pantalla.print(tempC);

Serial.print("\n");

delay(500);




if(stoptimer == true)
    {
      


String Mensaje;
     Mensaje = (String)"MONITOREO DE TEMPERATURA TERMINADO \n\n Su Resultado es \n\n Temperatura Corporal: " + tempC + (String)" \n\n NOTA: Verifica tu resultado con los rangos de temperatura establecidos.";
     
bot.sendMessage( CHAT_ID,Mensaje, "");
     contador =0;
     tiempo= 0;
//      display.clearDisplay();
//     display.setTextSize(2);                    
//     display.setTextColor(WHITE);             
//     display.setCursor(40,5);                
//     display.println("TEST"); 
//     display.setCursor(15,30);
//     display.println("TERMINADO");  
//     display.display();
      delay(2000);
//      pantalla.clear();
//      pantalla.setCursor(6,0);
//      pantalla.print("TEST");
//      pantalla.setCursor(3,1);
//      pantalla.print("TERMINADO");  
     Serial.print("Test Terminado");
     delay(3000);
     stoptimer = false;
      
    }

}
